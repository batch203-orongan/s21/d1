// console.log("Hello, World!")

// We want ro list the student ID of all graduateing student of the class.

let studentNumberA = "2020-1923";
let studentNumberB = "2020=1924";
let studentNumberC = "2020=1925";
let studentNumberD = "2020=1926";
let studentNumberE = "2020=1927";

//We can simply write the code above like this in array:
let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

// [SECTION] Arrays

/*
    - Arrays are used to store multiple related values in a single variable.
    - They are declared using square brackets ([]) also known as "Array Literals"
    - Arrays also provided access to a number of functions/methods that help in manipulation array.
        - Methods are used to manipulate information stored within the same object.
    -Arrays are also objects which is another data type.
    -The main difference of arrays with objects is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).
    -Syntax:
        let/const arrayName = [elementA, elementB, elementC, ...,];

*/ 

// Common examples of arrays
let grades = [98.5, 94.3, 89.2, 90];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

console.log(grades);
console.log(computerBrands);

// Possible use of an array but it is not recommended
let mixedArray = ["John", "Doe", 12, false, null, undefined, {}];
console.log(mixedArray);

// Alternative way to write array

let myTasks = [
    "drink html",
    "eat javascript",
    "inhale css",
    "bake sass"
];

// Creating an array eith values from variable 
let city = "Tokyo"
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city, city2, city3];
console.log(cities);

//  Object values; let sampleObjects = [{obj1}, {obj2}];

// [SECTION] .length property
// ".length" property allows us to get and set the total number of items in an array.

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// lenght property can also be used in string. As well as some methods and properties can also be used with strings.

let fullName = "Emmanuel Orongan";
console.log(fullName.length); // space are counted as characters in strings.

// .length property can also set the total number of items in an array.
// console.log(myTasks.length);
// console.log(myTasks);
//4             =4-1
//3
myTasks.length = myTasks.length -1;
console.log(myTasks.length);
console.log(myTasks);

// To delete a specific item in an array we can employ array methods. We have only shown the logic or algorithm of the "pop method".

// Another example using decrement.
cities.length--;
console.log(cities);

// We can't do the same on strings.
fullName.length = fullName.length - 1;
console.log(fullName.length);
console.log(fullName);

// We can also add the length of an array.
let theBeatles = ["John", "Paul", "Ringo", "George"];
// theBeatles.length++;
            //index = 4
theBeatles[theBeatles.length] = "Cardo";
console.log(theBeatles);

// [SECTION] Reading from Arrays
    /*
        -Accessing arrays elements is one of the common task that we dowith an array.
        - This canbe done through the use if array indexes.
        - Each element is an array is associated with it's own index number.
        - The first index array is associated with the number 0, and increasing this number by 1 for every element.
        - Array indexes it is actually refer to a memory address/location.

        Array Address: 0x7ffe942bad0
        Array[0]= 0x7ffe942bad0
        Array[1]= 0x7ffe942bad4
        Array[2]= 0x7ffe942bad8

            Syntax:
                arrayName[index];
    */ 

    console.log(grades[0]);
    console.log(computerBrands[3]);

    // Accessing an array elements that does not exist it will return "undefined"
    console.log(grades[20]);

    let lakersLegends = ["Kobe","Shaq", "Lebron", "Magic", "Kareem"];
    console.log(lakersLegends[1]); //Shaq
    console.log(lakersLegends[3]); //Magic

    // You can also svae/store array items in another varible.
    let currentLaker = lakersLegends[2];
    console.log(currentLaker);
    
    // You can also reassign array values using the item's indicates.
    console.log("Array before rassignment");
    console.log(lakersLegends);

    lakersLegends[2] = "Gasol";
    console.log("Array after reassignment");
    console.log(lakersLegends);

    //Access the last element of an array
    // Since the firs element of an array starts a 0, subtracting 1 to the length of an array will offset the values by one allowing us to get rhe last element.

    let bullsLegends = ["Jorda", "Pippen", "Rodman", "Rose", "Kukoc"];
    let lastElementIndex = bullsLegends.length-1;
    console.log(bullsLegends[lastElementIndex]);
    // console.log(bullsLegends[bullsLegends.length-1]);

    //adding Items into an Array

    // we can add items in an array using indices.
    const newArr = [];
    console.log(newArr[0]); //undefined

    newArr[0] = "Cloud Strife";
    console.log(newArr);

    console.log(newArr[1]);
    newArr[1] = "Tifa Lockhart"
    console.log(newArr);

    // we can add items at the end of the array. using the array.length

    // newArr[newArr.length-1] = "Barret Wallace";// reassigns the value of the last element.
    newArr[newArr.length] = "Barret Wallace";
    console.log(newArr);

    // Looping over an array
    // You can use a for loop to iterate over all items in an array.

    for(let index = 0; index < newArr.length; index++){
        // To be able to show each array items in the console.log
        console.log(newArr[index]);
    }

    // Create a programm that will filter the array of numbers which are divisible by 5.

    let numArr = [5, 12, 30, 46, 40, 52];

    for(let i = 0; i < numArr.length; i++){
        if(numArr[i] % 5 === 0){
            console.log(numArr[i] + " is divisible by 5");
        }
        else{
            console.log(numArr[i] + " is not divisible by 5");
        }
    }

// [SECTION] Multidimensional Arrays
/*
    - Multidimensional array are useful for storing complex data structures.
    - A practical application of this is to help visualize/create real world objects.
    - This is frequently  used to store datea for mathematic computations, image processing, and record management.
    - Array within an array
*/
//Create a chessboard

let chessBoard = [
    ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
    ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
    ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
    ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
    ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
    ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
    ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
    ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.table(chessBoard);

// Access a element of a multidimensional array.
// Syntax: multiArr[outerArr][innerArr]
console.log(chessBoard[3][4]);

console.log("Pawn moves to: "+chessBoard[2][5]);